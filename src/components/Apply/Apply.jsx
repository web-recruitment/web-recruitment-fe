import React, { useState, useEffect } from "react";
import ApplyApi from "../Axios/ApplyApi";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { Button, ButtonBase } from "@mui/material";
import Dialog from "@mui/material/Dialog";
import DialogTitle from "@mui/material/DialogTitle";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogActions from "@mui/material/DialogActions";
import "../Apply/Apply.css";

const makeStatus = (status) => {
  if (status === "DONE") {
    return {
      background: "rgb(145 254 159 / 47%)",
      color: "green",
    };
  } else if (status === "REQUEST") {
    return {
      background: "#ffadad8f",
      color: "red",
    };
  } else if (status === "INACTIVE") {
    return {
      background: "#ffadad8f",
      color: "black",
    };
  }
};
export default function Apply(ApplyItems) {
  const [apply, setApply] = useState([]);
  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = async () => {
    try {
      const response = await ApplyApi.getAllApply();
      setApply(response.data.data.slice(0, 10));
      console.log(response.data.data);
    } catch (error) {
      console.error(error);
    }
  };
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  return (
    <div className="Table">
      <h3>Apply Company</h3>
      <TableContainer
        component={Paper}
        style={{ boxShadow: "0px 13px 20px 0px #80808029" }}
      >
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Apply ID</TableCell>
              <TableCell align="left">Cv ID</TableCell>
              <TableCell align="left">Date</TableCell>
              <TableCell align="left">Status</TableCell>
              <TableCell align="left">Information</TableCell>
            </TableRow>
          </TableHead>
          <TableBody style={{ color: "white" }}>
            {
              (ApplyItems = apply.map((applys) => (
                <TableRow
                  key={applys.operationId || applys.companyId}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {applys.operationId}
                  </TableCell>
                  <TableCell align="left">{applys.responseCv.cvId}</TableCell>
                  <TableCell align="left">{applys.date}</TableCell>
                  <TableCell align="left">
                    <span className="status" style={makeStatus(applys.status)}>
                      {applys.status}
                    </span>
                  </TableCell>
                  <TableCell align="left" className="Details">
                    <ButtonBase onClick={handleOpen}>SHOW</ButtonBase>
                    <Dialog open={open} onClose={handleClose}>
                      <DialogTitle>INFORMATION DETAILS</DialogTitle>
                      <DialogContent>
                        <DialogContentText>
                          This is the content of the pop-up.
                        </DialogContentText>
                      </DialogContent>
                      <DialogActions>
                        <Button onClick={handleClose} color="primary">
                          Close
                        </Button>
                      </DialogActions>
                    </Dialog>
                  </TableCell>
                </TableRow>
              )))
            }
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
