import React, { useState, useEffect } from "react";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import "../StaffCompany/Staff.css";
import AccountApi from "../Axios/AccountApi";
import { ButtonBase } from "@mui/material";

const makeStyle = (status) => {
  if (status === "ACTIVE" || status === "ACCPET") {
    return {
      background: "rgb(145 254 159 / 47%)",
      color: "green",
    };
  } else if (status === "REQUEST" || status === "INACTIVE") {
    return {
      background: "#ffadad8f",
      color: "red",
    };
  } else {
    return {
      background: "#59bfff",
      color: "white",
    };
  }
};
const makeRole = (role) => {
  if (role === "ADMIN") {
    return {
      background: "#ffadad8f",
      color: "red",
    };
  } else if (role === "COMPANY") {
    return {
      background: "#59bfff",
      color: "blue",
    };
  } else if (role === "CANDIDATE") {
    return {
      background: "rgb(145 254 159 / 47%)",
      color: "green",
    };
  }
};
export default function Staff(AccountItems) {
  const [accounts, setAccounts] = useState([]);
  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = async () => {
    try {
      const response = await AccountApi.getAllAccounts(5);
      setAccounts(response.data.data.slice(0, 10));
      console.log(response.data.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="Table">
      <h3>Accounts Company</h3>
      <TableContainer
        component={Paper}
        style={{ boxShadow: "0px 13px 20px 0px #80808029" }}
      >
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Account ID</TableCell>
              <TableCell align="left">User Name</TableCell>
              <TableCell align="left">Full Name</TableCell>
              <TableCell align="left">Email</TableCell>
              <TableCell align="left">ROLE</TableCell>
              <TableCell align="left">Date</TableCell>
              <TableCell align="left">Status</TableCell>
              <TableCell align="left">Information</TableCell>
            </TableRow>
          </TableHead>
          <TableBody style={{ color: "white" }}>
            {
              (AccountItems = accounts.map((account) => (
                <TableRow
                  key={account.accountId}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {account.accountId}
                  </TableCell>
                  <TableCell align="left">{account.accountId}</TableCell>
                  <TableCell align="left">
                    {account.firstName} {account.lastName}
                  </TableCell>
                  <TableCell align="left">{account.email}</TableCell>
                  <TableCell align="left">
                    <span className="status" style={makeRole(account.role)}>
                      {account.role}
                    </span>
                  </TableCell>
                  <TableCell align="left">{account.date}</TableCell>
                  <TableCell align="left">
                    <span className="status" style={makeStyle(account.status)}>
                      {account.status}
                    </span>
                  </TableCell>

                  <TableCell align="left" className="Details">
                    <ButtonBase>SHOW</ButtonBase>
                  </TableCell>
                </TableRow>
              )))
            }
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
