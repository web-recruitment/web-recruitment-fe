import React, { useState, useEffect } from "react";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import "../Job/Job.css";
import JobsApi from "../Axios/JobsApi";
import { ButtonBase } from "@mui/material";

const makeStatus = (status) => {
  if (status === "ACCEPT") {
    return {
      background: "rgb(145 254 159 / 47%)",
      color: "green",
    };
  } else if (status === "INACTIVE") {
    return {
      background: "#ffadad8f",
      color: "red",
    };
  }
};
export default function Job(JobItems) {
  const [jobs, setJobs] = useState([]);
  useEffect(() => {
    fetchPosts();
  }, []);

  const fetchPosts = async () => {
    try {
      const response = await JobsApi.getAllJobs();
      setJobs(response.data.data.slice(0, 10));
      console.log(response.data.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="Table">
      <h3>Jobs Company</h3>
      <TableContainer
        component={Paper}
        style={{ boxShadow: "0px 13px 20px 0px #80808029" }}
      >
        <Table sx={{ minWidth: 650 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Job ID</TableCell>
              <TableCell align="left">Name Skill</TableCell>
              <TableCell align="left">Description</TableCell>
              <TableCell align="left">Status</TableCell>
              <TableCell align="left">Information</TableCell>
            </TableRow>
          </TableHead>
          <TableBody style={{ color: "white" }}>
            {
              (JobItems = jobs.map((job) => (
                <TableRow
                  key={job.jobId || job.companyId}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell component="th" scope="row">
                    {job.jobId}
                  </TableCell>
                  <TableCell align="left">{job.nameSkill}</TableCell>
                  <TableCell align="left">{job.description}</TableCell>
                  <TableCell align="left">
                    <span className="status" style={makeStatus(job.status)}>
                      {job.status}
                    </span>
                  </TableCell>
                  <TableCell align="left" className="Details">
                    <ButtonBase>SHOW</ButtonBase>
                  </TableCell>
                </TableRow>
              )))
            }
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
