import axiosApi from "./AxiosApi";

const ApplyApi = {
  getAllApply(page) {
    const url = "/Operations/GetOperations";
    const params = {
      page: page,
      limit: 5, // Limit to 5 data items per page
    };
    return axiosApi.get(url, { params });
  },
};
export default ApplyApi;
