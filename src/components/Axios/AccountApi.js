import axiosApi from "./AxiosApi";

const AccountApi = {
  getAllAccounts(page) {
    const url = "/Accounts/GetAccounts";
    const params = {
      page: page,
      limit: 5, // Limit to 5 data items per page
    };
    return axiosApi.get(url, { params });
  },
};
export default AccountApi;
