import axiosApi from "./AxiosApi";

const JobsApi = {
  getAllJobs(page) {
    const url = "/Jobs/GetAllJobs";
    const params = {
      page: page,
      limit: 5, // Limit to 5 data items per page
    };
    return axiosApi.get(url, { params });
  },
};
export default JobsApi;
